#' Simulation of an epidemic with household and workplace communiteies
#'
#' @param parameters named list with parameter values
#' @param dataframe with population information
#' @param clocks value of times in the different states
#' @param proportion of the population changing status for output
#' @param rate_functions a vector specifying if rate is linear or sqrt
#'
#' @examples
#' simulate_epidemic(list(beta_i_i=2, param_ir=1), data.frame(index=1:100, C=sample(c(0, 1), prob=c(0.9, 0.1), replace=TRUE, size=100), H=1:100, W=1:100))
#'
#' @return named list 
#' @useDynLib communityEpidemics
#' @export
simulate_epidemic <- function(parameters, population, clocks=NULL, batch=0.01, itermax=1e8, tmax=1e3, rate_functions=c("lin", "lin", "lin")){

    if (batch>0 & batch<1){
        batchsize <- max(floor(batch*dim(population)[1]), 1)
    } else {
        warning("Incorrect batch value, defaulting to 0.01")
        batchsize <- max(floor(0.01*dim(population)[1]), 1)
    }

    # Check values of state (between 0 and  4)
    # Check max value of structures
    # Check positivity of parameters

    params <- list(beta_i_i=0, beta_i_ih=0, beta_i_iw=0,
        beta_e_i=0, beta_e_ih=0, beta_e_iw=0,
        beta_e_a=0, beta_e_ah=0, beta_e_aw=0,
        param_ei=0, param_ea=0, param_ir=0, param_ar=0,
        tmax=tmax, itermax=as.integer(itermax), batchsize=as.integer(batchsize))
    params[names(parameters)]=as.numeric(parameters)

    rate_funcs <- as.integer(sapply(rate_functions, function(type){
        switch(type, "lin"=0, "sqrt"=1, 0)
        }))

    data.frame(.Call("simulate_seair", params, population, clocks, rate_funcs))
}


#' Get the value of R0 for SIR epidemic with dual substructures
#'
#' @param beta infection parameter for the mean field
#' @param betaH infection parameter for the household
#' @param betaW infection parameter for the workplace
#' @param gamma recovery rate
#' @param p household size distribution
#' @param q workplace size distribution
#' @return value of R0
#'
#' @examples
#' community_r0(1.5, 0.5, 0.5, 1, c(0, 0, 1), c(0, 0.5, 0, 0.5))
#'
#' @export
community_r0 <- function(beta, betaH, betaW, gamma, p, q,
    nH = NULL, nW = NULL, repet=10000){

   mH <- sum(seq_along(p) * p)
   phat <- seq_along(p) * p / mH
   if (is.null(nH)){
      nH <- sapply(seq_along(p), community_epidemic_size, betaH, gamma, repet=repet)
   }
   nHhat <- sum(phat * (nH - 1))

   mW <- sum(seq_along(q) * q)
   qhat<- seq_along(q) * q / mW
   if(is.null(nW)){
      nW <- sapply(seq_along(q), community_epidemic_size, betaW, gamma, repet=repet)
   }
   nWhat <- sum(qhat * (nW - 1))

   A <- matrix(c(
      beta/gamma, beta/gamma, beta/gamma,
      nHhat, 0, nHhat,
      nWhat, nWhat, 0
   ), ncol = 3, nrow = 3, byrow = T)
   eig <- eigen(A)
   p <- eig$vectors[, which.max(eig$values)] / sum(eig$vectors[ , which.max(eig$values)])

   R0 <- beta / gamma + (1-p[2]) * nHhat + (1-p[3]) * nWhat
   return(list(R0=R0, pM=p[1], pH=p[2], pW=p[3],
        nWhat=nWhat, nHhat=nHhat))
}


#' Compute by simulation the average number of cases in an isolated structure
#'
#' @param k the size of the structure
#' @param beta the infection parameter
#' @param gamma the recovery rate
#' @param number of repetitions
#' @return average number of cases in the structure
#'
#' @export
community_epidemic_size <- function(k, beta, gamma, repet=10000){
   S <- NULL
   N <- k
   for (j in 1:repet){
      t <-rexp(N, rate=gamma)
      q <- c(0, sort(rexp(N-1, rate=1), decreasing=FALSE))
      s <- 1
      while (s<N & q[s+1]<beta*sum(t[1:s])) {
        s=s+1
      }
      S[j] <- s
   }
   return(mean(S))
}
