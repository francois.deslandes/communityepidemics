#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h" 

struct individuals initParameters(double beta, double betaH, double betaW, double gamma){
	int i;
	struct individuals ind;	

	ind.beta=beta;
	ind.betaH=betaH;
	ind.betaW=betaW;
	ind.gamma=gamma;

    // Init infection rate function
    ind.infection_rate_household = INFECTION_LIN;
    ind.infection_rate_workplace = INFECTION_LIN;
    ind.infection_rate_general = INFECTION_LIN;

	return ind;
}

struct individuals initParametersSEAIR(double beta_i_i, double beta_i_ih, double beta_i_iw,
    double beta_e_i, double beta_e_ih, double beta_e_iw,
    double beta_e_a, double beta_e_ah, double beta_e_aw,
    double param_ei, double param_ea,
    double param_ir, double param_ar){
    struct individuals ind;

	ind.beta=beta_i_i;
	ind.betaH=beta_i_ih;
	ind.betaW=beta_i_iw;
	ind.gamma=param_ir;

    ind.beta_i_i=beta_i_i;
    ind.beta_i_ih=beta_i_ih;
    ind.beta_i_iw=beta_i_iw;

    ind.beta_e_i=beta_e_i;
    ind.beta_e_ih=beta_e_ih;
    ind.beta_e_iw=beta_e_iw;

    ind.beta_e_a=beta_e_a;
    ind.beta_e_ah=beta_e_ah;
    ind.beta_e_aw=beta_e_aw;

    ind.param_ei=param_ei;
    ind.param_ea=param_ea;

    ind.param_ir=param_ir;
    ind.param_ar=param_ar;

    // Clocked events
    ind.with_clocks=0;

    // Init infection rate function
    ind.infection_rate_household = INFECTION_LIN;
    ind.infection_rate_workplace = INFECTION_LIN;
    ind.infection_rate_general = INFECTION_LIN;

    return ind;
}

struct individuals fileInitParametersSEAIR(char file[]){
    FILE* fd;
    struct individuals ind;
    char clocked_events[10];

    if((fd=fopen(file, "r"))==NULL){
        fprintf(stderr, "Could not open file : %s\n", file);
        exit(EXIT_FAILURE);
    }

    fscanf(fd, "infectionGHW_by_I_to_I: %lf %lf %lf\n",
        &ind.beta_i_i, &ind.beta_i_ih, &ind.beta_i_iw);
	ind.beta=ind.beta_i_i;
	ind.betaH=ind.beta_i_ih;
	ind.betaW=ind.beta_i_iw;

    fscanf(fd, "infectionGHW_by_I_to_E: %lf %lf %lf\n",
        &ind.beta_e_i, &ind.beta_e_ih, &ind.beta_e_iw);

    fscanf(fd, "infectionGHW_by_A_to_E: %lf %lf %lf\n",
        &ind.beta_e_a, &ind.beta_e_ah, &ind.beta_e_aw);

    fscanf(fd, "transition_E_to_I: %lf\n",
        &ind.param_ei);

    fscanf(fd, "transition_E_to_A: %lf\n",
        &ind.param_ea);

    fscanf(fd, "transition_I_to_R: %lf\n",
        &ind.param_ir);
    ind.gamma=ind.param_ir;

    fscanf(fd, "transition_A_to_R: %lf\n",
        &ind.param_ar);

    fscanf(fd, "clocked_events: %s\n",
        clocked_events);
    if(strcmp(clocked_events, "true")==0){
        ind.with_clocks=1;
    } else {
        ind.with_clocks=0;
    }

    fclose(fd);

    // Init infection rate function
    ind.infection_rate_household = INFECTION_LIN;
    ind.infection_rate_workplace = INFECTION_LIN;
    ind.infection_rate_general = INFECTION_LIN;

    return ind;
}

void freeIndividuals(struct individuals *ind){
	int i;

	free(ind->index);
	free(ind->state);

	//Generation counting
	free(ind->generation);

	//Structure Generation counting
	free(ind->generationStructure);
	free(ind->generationStructureType);

	for(i=0; i<NEVENTS; i++){
		free(ind->rates[i]);
	}

	for(i=0; i<NSTATES; i++){
		free(ind->nh[i]);
		free(ind->nw[i]);
	}

	// Generation counting
	// Strucure Generation counting
	for(i=0; i<NGEN; i++){
		free(ind->genh[i]);
		free(ind->genw[i]);
    }
	for(i=0; i<NGEN*3; i++){
		free(ind->genhStructure[i]);
		free(ind->genwStructure[i]);
	}

	free(ind->h);
	free(ind->w);

	if(ind->households.max>0){
		for(i=0; i<ind->households.max; i++){
			free(ind->households.ind[i]);
		}
		free(ind->households.ind);
	}
    free(ind->households.N);

	if(ind->workplaces.max>0){
		for(i=0; i<ind->workplaces.max; i++){
			free(ind->workplaces.ind[i]);
		}
		free(ind->workplaces.ind);
	}
    free(ind->workplaces.N);

    // Clocked events
    free(ind->infection_time);
    free(ind->duration_ei);
    free(ind->duration_ea);
    free(ind->duration_ir);
    free(ind->duration_ar);
    freeListCE(&ind->clocked_event_list);
}


void readPop(char *file, struct individuals *ind){
	size_t i, j, k, id, state, h, w, N;
    double time_ei, time_ea, time_ir, time_ar;
	size_t maxh=0, maxw=0;
	short gen, type;
	FILE* fd=fopen(file, "r");
	char line[MAXLINE];

	// Count number of individuals
	fgets(line, MAXLINE, fd); //header
	N=0;
	while(fgets(line, MAXLINE, fd)){
		N++;
	}
	rewind(fd);

    // Init infection rate function
    ind->infection_rate_household = INFECTION_LIN;
    ind->infection_rate_workplace = INFECTION_LIN;
    ind->infection_rate_general = INFECTION_LIN;

	// alloc individuals
	ind->N=N;
	ind->index=malloc(N*sizeof(size_t));
	ind->state=malloc(N*sizeof(short));

    // Clocked events
	ind->infection_time=malloc(N*sizeof(double));
	ind->duration_ei=malloc(N*sizeof(double));
	ind->duration_ea=malloc(N*sizeof(double));
	ind->duration_ir=malloc(N*sizeof(double));
	ind->duration_ar=malloc(N*sizeof(double));
    initListCE(&ind->clocked_event_list, 2*N, 4*N);

	// Generation counting
	ind->generation=malloc(N*sizeof(short));

	// Structure Generation counting
	ind->generationStructure=malloc(N*sizeof(short));
	ind->generationStructureType=malloc(N*sizeof(short));

	for(i=0; i<NEVENTS; i++){
		ind->rates[i]=malloc(N*sizeof(double));
	}

	for(i=0; i<NSTATES; i++){
		ind->nh[i]=malloc(N*sizeof(size_t));
		memset(ind->nh[i], 0, N*sizeof(size_t));
		ind->nw[i]=malloc(N*sizeof(size_t));
		memset(ind->nw[i], 0, N*sizeof(size_t));
	}
	ind->h=malloc(N*sizeof(size_t));
	ind->w=malloc(N*sizeof(size_t));

	// Init individuals from file
	for(i=0; i<NSTATES; i++){
		ind->counts[i]=0;
	}

	// Generation counting
	// Structure Generation counting
	for(i=0; i<NGEN; i++){
		ind->genCounts[i]=0;

		ind->genh[i]=malloc(N*sizeof(size_t));
		memset(ind->genh[i], 0, N*sizeof(size_t));

		ind->genw[i]=malloc(N*sizeof(size_t));
		memset(ind->genw[i], 0, N*sizeof(size_t));
	}

	// Structure Generation counting
	for(i=0; i<NGEN; i++){
		for(k=0; k<3; k++){
			ind->genCountsStructure[i+NGEN*k]=0;

			ind->genhStructure[i+NGEN*k]=malloc(N*sizeof(size_t));
			memset(ind->genhStructure[i+NGEN*k], 0, N*sizeof(size_t));

			ind->genwStructure[i+NGEN*k]=malloc(N*sizeof(size_t));
			memset(ind->genwStructure[i+NGEN*k], 0, N*sizeof(size_t));
		}
	}

	fgets(line, MAXLINE, fd); // header
	for(i=0; i<ind->N; i++){
		fgets(line, MAXLINE, fd);

        // Clocked events
        if(ind->with_clocks){
		    sscanf(line, "%zu;%zu;%zu;%zu;%lf;%lf;%lf;%lf", &id, &state, &h, &w, &time_ei, &time_ea, &time_ir, &time_ar);
            ind->duration_ei[id-1]=time_ei;
            ind->duration_ea[id-1]=time_ea;
            ind->duration_ir[id-1]=time_ir;
            ind->duration_ar[id-1]=time_ar;
        } else {
		    sscanf(line, "%zu;%zu;%zu;%zu", &id, &state, &h, &w);
            ind->duration_ei[id-1]=-1;
            ind->duration_ea[id-1]=-1;
            ind->duration_ir[id-1]=-1;
            ind->duration_ar[id-1]=-1;
        }
		ind->index[id-1]=id-1;
		ind->state[id-1]=state;
		ind->h[id-1]=h;
		ind->w[id-1]=w;

        //Clocked event
        if(state==STATE_I && ind->duration_ir[id-1]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ir[id-1], id-1, EVENT_IR);
        }
        if(state==STATE_A && ind->duration_ar[id-1]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ar[id-1], id-1, EVENT_AR);
        }
        if(state==STATE_E && ind->duration_ei[id-1]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ei[i], id-1, EVENT_EI);
            if(ind->duration_ir[id-1]>0)
                insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ei[id-1]+ind->duration_ir[id-1], id-1, EVENT_IR);
        }
        if(state==STATE_E && ind->duration_ea[id-1]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ea[id-1], id-1, EVENT_EA);
            if(ind->duration_ar[id-1]>0)
                insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ea[id-1]+ind->duration_ar[id-1], id-1, EVENT_AR);
        }
             
		// Generation counting
		// Structure Generation counting
		if(state==STATE_I){
			ind->generation[id-1]=0;
			ind->generationStructure[id-1]=0;
			ind->generationStructureType[id-1]=INDIV_CM;
		} else {
			ind->generation[id-1]=-1;
			ind->generationStructure[id-1]=-1;
			ind->generationStructureType[id-1]=-1;
		}

		if(h>maxh){
			maxh=h;
		} 
		if(w>maxw){
			maxw=w;
		}

		ind->counts[state]++;
	}

	// Alloc structures
	ind->households.max=maxh+1;
	ind->households.N=malloc((maxh+1)*sizeof(size_t));
	memset(ind->households.N, 0, (maxh+1)*sizeof(size_t));
	ind->households.ind=malloc((maxh+1)*sizeof(size_t));
	for(i=0; i<maxh+1; i++){
		ind->households.ind[i]=malloc(MAXSTRUCT*sizeof(size_t));
	}

	ind->workplaces.max=maxw+1;
	ind->workplaces.N=malloc((maxw+1)*sizeof(size_t));
	memset(ind->workplaces.N, 0, (maxw+1)*sizeof(size_t));
	ind->workplaces.ind=malloc((maxw+1)*sizeof(size_t));
	for(i=0; i<maxw+1; i++){
		ind->workplaces.ind[i]=malloc(MAXSTRUCT*sizeof(size_t));
	}

	// Init structures
	for(i=0; i<ind->N; i++){
		h=ind->h[i];
		ind->households.ind[h][ind->households.N[h]]=i;
		ind->households.N[h]++;

		w=ind->w[i];
		ind->workplaces.ind[w][ind->workplaces.N[w]]=i;
		ind->workplaces.N[w]++;
	}


	// Init individual H/W state counts
	for(i=0; i<ind->N; i++){
		h=ind->h[i];
		w=ind->w[i];
		for(state=0; state<NSTATES; state++){
			ind->nh[state][i]=0;
			ind->nw[state][i]=0;
		}

		// Generation counting (global)
		if(ind->generation[i]>=0 && ind->generation[i]<NGEN){
			ind->genCounts[ind->generation[i]]++;
		}
		for(gen=0; gen<NGEN; gen++){
			ind->genh[gen][i]=0;
			ind->genw[gen][i]=0;
		}

		// Structure Generation counting (global)
		if(ind->generationStructure[i]>=0 && ind->generationStructure[i]<NGEN){
			type=ind->generationStructureType[i];
			ind->genCountsStructure[ind->generationStructure[i]+type*NGEN]++;
		}
		for(gen=0; gen<NGEN; gen++){
			ind->genh[gen][i]=0;
			ind->genw[gen][i]=0;
		}

		// Household
		for(j=0; j<getHSize(ind, i); j++){
			state=getHState(ind, h, j);
			ind->nh[state][i]++;

			// Generation counting
			if(ind->generation[j]>=0){
				ind->genh[ind->generation[j]][i]++;
			}

			// Structure Generation counting
			if(ind->generationStructure[j]>=0){
				type=ind->generationStructureType[j];
				ind->genhStructure[ind->generationStructure[j]+type*NGEN][i]++;
			}
		}

		// Workplace
		for(j=0; j<getWSize(ind, i); j++){
			state=getWState(ind, w, j);
			ind->nw[state][i]++;

			// Generation counting
			if(ind->generation[j]>=0){
				ind->genw[ind->generation[j]][i]++;
			}

			// Structure Generation counting
			if(ind->generationStructure[j]>=0){
				type=ind->generationStructureType[j];
				ind->genwStructure[ind->generationStructure[j]+type*NGEN][i]++;
			}
		}
	}

	fclose(fd);
}

void initPop(struct individuals *ind, int N, int *id, int *state, int *h, int *w,
    double *clock_time_ei, double *clock_time_ea, double *clock_time_ir, double *clock_time_ar){
	int i, j, k, hi, wi, statei;
    double time_ei, time_ea, time_ir, time_ar;
	size_t maxh=0, maxw=0;
	short gen, type;

    // Init infection rate function
    ind->infection_rate_household = INFECTION_LIN;
    ind->infection_rate_workplace = INFECTION_LIN;
    ind->infection_rate_general = INFECTION_LIN;

	// alloc individuals
	ind->N=N;
	ind->index=malloc(N*sizeof(size_t));
	ind->state=malloc(N*sizeof(short));

    // Clocked events
	ind->infection_time=malloc(N*sizeof(double));
	ind->duration_ei=malloc(N*sizeof(double));
	ind->duration_ea=malloc(N*sizeof(double));
	ind->duration_ir=malloc(N*sizeof(double));
	ind->duration_ar=malloc(N*sizeof(double));
    initListCE(&ind->clocked_event_list, 2*N, 4*N);


	// Generation counting
	ind->generation=malloc(N*sizeof(short));

	// Structure Generation counting
	ind->generationStructure=malloc(N*sizeof(short));
	ind->generationStructureType=malloc(N*sizeof(short));

	for(i=0; i<NEVENTS; i++){
		ind->rates[i]=malloc(N*sizeof(double));
	}

	for(i=0; i<NSTATES; i++){
		ind->nh[i]=malloc(N*sizeof(size_t));
		memset(ind->nh[i], 0, N*sizeof(size_t));
		ind->nw[i]=malloc(N*sizeof(size_t));
		memset(ind->nw[i], 0, N*sizeof(size_t));
	}
	ind->h=malloc(N*sizeof(size_t));
	ind->w=malloc(N*sizeof(size_t));

	// Init individuals from file
	for(i=0; i<NSTATES; i++){
		ind->counts[i]=0;
	}

	// Generation counting
	// Structure Generation counting
	for(i=0; i<NGEN; i++){
		ind->genCounts[i]=0;

		ind->genh[i]=malloc(N*sizeof(size_t));
		memset(ind->genh[i], 0, N*sizeof(size_t));

		ind->genw[i]=malloc(N*sizeof(size_t));
		memset(ind->genw[i], 0, N*sizeof(size_t));
	}

	// Structure Generation counting
	for(i=0; i<NGEN; i++){
		for(k=0; k<3; k++){
			ind->genCountsStructure[i+NGEN*k]=0;

			ind->genhStructure[i+NGEN*k]=malloc(N*sizeof(size_t));
			memset(ind->genhStructure[i+NGEN*k], 0, N*sizeof(size_t));

			ind->genwStructure[i+NGEN*k]=malloc(N*sizeof(size_t));
			memset(ind->genwStructure[i+NGEN*k], 0, N*sizeof(size_t));
		}
	}

	for(i=0; i<ind->N; i++){
        // Clocked events
        if(ind->with_clocks){
		    //sscanf(line, "%zu;%zu;%zu;%zu;%lf;%lf;%lf;%lf", &id, &state, &h, &w, &time_ei, &time_ea, &time_ir, &time_ar);
            ind->duration_ei[i]=clock_time_ei[i];
            ind->duration_ea[i]=clock_time_ea[i];
            ind->duration_ir[i]=clock_time_ir[i];
            ind->duration_ar[i]=clock_time_ar[i];
        } else {
		    //sscanf(line, "%zu;%zu;%zu;%zu", &id, &state, &h, &w);
            ind->duration_ei[i]=-1;
            ind->duration_ea[i]=-1;
            ind->duration_ir[i]=-1;
            ind->duration_ar[i]=-1;
        }
		ind->index[i]=id[i];
		ind->state[i]=state[i];
		ind->h[i]=h[i];
		ind->w[i]=w[i];

        //Clocked event
        if(state[i]==STATE_I && ind->duration_ir[i]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ir[i], i, EVENT_IR);
        }
        if(state[i]==STATE_A && ind->duration_ar[i]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ar[i], i, EVENT_AR);
        }
        if(state[i]==STATE_E && ind->duration_ei[i]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ei[i], i, EVENT_EI);
            if(ind->duration_ir[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ei[i]+ind->duration_ir[i], i, EVENT_IR);
        }
        if(state[i]==STATE_E && ind->duration_ea[i]>0){
            insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ea[i], i, EVENT_EA);
            if(ind->duration_ar[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, ind->duration_ea[i]+ind->duration_ar[i], i, EVENT_AR);
        }
             
		// Generation counting
		// Structure Generation counting
		if(state[i]==STATE_I){
			ind->generation[i]=0;
			ind->generationStructure[i]=0;
			ind->generationStructureType[i]=INDIV_CM;
		} else {
			ind->generation[i]=-1;
			ind->generationStructure[i]=-1;
			ind->generationStructureType[i]=-1;
		}

		if(h[i]>maxh){
			maxh=h[i];
		} 
		if(w[i]>maxw){
			maxw=w[i];
		}

		ind->counts[state[i]]++;
	}


	// Alloc structures
	ind->households.max=maxh+1;
	ind->households.N=malloc((maxh+1)*sizeof(size_t));
	memset(ind->households.N, 0, (maxh+1)*sizeof(size_t));
	ind->households.ind=malloc((maxh+1)*sizeof(size_t));
	for(i=0; i<maxh+1; i++){
		ind->households.ind[i]=malloc(MAXSTRUCT*sizeof(size_t));
	}

	ind->workplaces.max=maxw+1;
	ind->workplaces.N=malloc((maxw+1)*sizeof(size_t));
	memset(ind->workplaces.N, 0, (maxw+1)*sizeof(size_t));
	ind->workplaces.ind=malloc((maxw+1)*sizeof(size_t));
	for(i=0; i<maxw+1; i++){
		ind->workplaces.ind[i]=malloc(MAXSTRUCT*sizeof(size_t));
	}

	// Init structures
	for(i=0; i<ind->N; i++){
		hi=ind->h[i];
		ind->households.ind[hi][ind->households.N[hi]]=i;
		ind->households.N[hi]++;

		wi=ind->w[i];
		ind->workplaces.ind[wi][ind->workplaces.N[wi]]=i;
		ind->workplaces.N[wi]++;
	}

	// Init individual H/W state counts
	for(i=0; i<ind->N; i++){
		hi=ind->h[i];
		wi=ind->w[i];
		for(statei=0; statei<NSTATES; statei++){
			ind->nh[statei][i]=0;
			ind->nw[statei][i]=0;
        }

		// Generation counting (global)
		if(ind->generation[i]>=0 && ind->generation[i]<NGEN){
			ind->genCounts[ind->generation[i]]++;
		}
		for(gen=0; gen<NGEN; gen++){
			ind->genh[gen][i]=0;
			ind->genw[gen][i]=0;
		}

		// Structure Generation counting (global)
		if(ind->generationStructure[i]>=0 && ind->generationStructure[i]<NGEN){
			type=ind->generationStructureType[i];
			ind->genCountsStructure[ind->generationStructure[i]+type*NGEN]++;
		}
		for(gen=0; gen<NGEN; gen++){
			ind->genh[gen][i]=0;
			ind->genw[gen][i]=0;
		}

		// Household
		for(j=0; j<getHSize(ind, i); j++){
			statei=getHState(ind, hi, j);
			ind->nh[statei][i]++;

			// Generation counting
			if(ind->generation[j]>=0){
				ind->genh[ind->generation[j]][i]++;
			}

			// Structure Generation counting
			if(ind->generationStructure[j]>=0){
				type=ind->generationStructureType[j];
				ind->genhStructure[ind->generationStructure[j]+type*NGEN][i]++;
			}
		}

		// Workplace
		for(j=0; j<getWSize(ind, i); j++){
			statei=getWState(ind, wi, j);
			ind->nw[statei][i]++;

			// Generation counting
			if(ind->generation[j]>=0){
				ind->genw[ind->generation[j]][i]++;
			}

			// Structure Generation counting
			if(ind->generationStructure[j]>=0){
				type=ind->generationStructureType[j];
				ind->genwStructure[ind->generationStructure[j]+type*NGEN][i]++;
			}
		}
	}
}

size_t getHSize(struct individuals *ind, size_t i){
	size_t h=ind->h[i];
	return ind->households.N[h];
}

size_t getWSize(struct individuals *ind, size_t i){
	size_t w=ind->w[i];
	return ind->workplaces.N[w];
}

short getHState(struct individuals *ind, size_t h, size_t j){
	size_t index=ind->households.ind[h][j];
	return ind->state[index];
}

short getWState(struct individuals *ind, size_t w, size_t j){
	size_t index=ind->workplaces.ind[w][j];
	return ind->state[index];
}


void print(struct individuals *ind){
	size_t k;
	fprintf(stdout, "Ind State H W HS HI HR WS WI WR\n");
	for(k=0; k<ind->N; k++){
		fprintf(stdout, "%zu ", ind->index[k]);
		fprintf(stdout, "%d ", ind->state[k]);
		fprintf(stdout, "%zu ", ind->h[k]);
		fprintf(stdout, "%zu ", ind->w[k]);
		fprintf(stdout, "%zu ", ind->nh[STATE_S][k]);
		fprintf(stdout, "%zu ", ind->nh[STATE_I][k]);
		fprintf(stdout, "%zu ", ind->nh[STATE_R][k]);
		fprintf(stdout, "%zu ", ind->nw[STATE_S][k]);
		fprintf(stdout, "%zu ", ind->nw[STATE_I][k]);
		fprintf(stdout, "%zu ", ind->nw[STATE_R][k]);
		fprintf(stdout, "\n");
	}
}

void printSummary(struct individuals *ind, double t){
	size_t k;
	fprintf(stdout, "%lf ", t);
	for(k=0; k<NSTATES; k++){
		fprintf(stdout, "%zu ", ind->counts[k]);
	}
	fprintf(stdout, "\n");
}

void printStructures(struct individuals *ind){
	size_t h, w, k;
	fprintf(stdout, "=== Households ===\n");
	for(h=0; h<ind->households.max; h++){
		fprintf(stdout, "%zu : ", h);
		for(k=0; k<ind->households.N[h]; k++){
			fprintf(stdout, "%zu ", ind->households.ind[h][k]);
		}
		fprintf(stdout, "\n");
	}

	fprintf(stdout, "=== Workplaces ===\n");
	for(h=0; h<ind->workplaces.max; h++){
		fprintf(stdout, "%zu : ", h);
		for(k=0; k<ind->workplaces.N[h]; k++){
			fprintf(stdout, "%zu ", ind->workplaces.ind[h][k]);
		}
		fprintf(stdout, "\n");
	}
}

void writeDyn(struct individuals *ind, double t, FILE* fd){
	size_t k;
	for(k=0; k<NSTATES; k++){
		fprintf(fd, "%zu;", ind->counts[k]);
	}
	fprintf(fd, "%lf;0", t); // zero is for padding only
	for(k=0; k<NEVENTS; k++){
		fprintf(fd, ";%lf", ind->ratesByEvent[k]);
	}
	fprintf(fd, "\n");
}

void set_infection_rate_functions(struct individuals *ind, char general, char household, char workplace){

    // Init infection rate function
    ind->infection_rate_household = household;
    ind->infection_rate_workplace = workplace;
    ind->infection_rate_general = general;
}
