#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "clocked.h"


void initListCE(struct list_ce *l, size_t max_size, size_t max_capacity){
    int i;

    l->size=0;
    l->next_slot=0;
    l->max_size=max_size;
    l->max_capacity=max_capacity;
    l->head=NULL;
    l->slots=(struct clocked_event*)malloc(max_size*sizeof(struct clocked_event));
    for(i=0; i<max_size; i++){
        l->slots[i].isset=0;
    }
}

void freeListCE(struct list_ce *l){
    free(l->slots);
}

void popCE(struct list_ce *l, double *time, size_t *indiv, size_t *event){
    if(l->size==0){
        fprintf(stderr, "Cannot remove element from empty list\n");
        exit(EXIT_FAILURE);
    }
    struct clocked_event *next=l->head->next;
    *time=l->head->time;
    *indiv=l->head->indiv;
    *event=l->head->event;
    l->head->next=NULL;
    l->head->isset=0;
    l->head=next;
    l->size--;
}

void insertIncreasingListCE(struct list_ce *l, double time, size_t indiv, size_t event){
    int new_size;

    // Insert in correct position
    struct clocked_event *current = l->head;
    if(l->size==0){
        l->slots[l->next_slot].time=time;
        l->slots[l->next_slot].indiv=indiv;
        l->slots[l->next_slot].event=event;
        l->slots[l->next_slot].isset=1;
        l->slots[l->next_slot].next=NULL;
        l->head=l->slots+l->next_slot;
        l->size++;
    } else if(time < l->head->time){
        l->slots[l->next_slot].time=time;
        l->slots[l->next_slot].indiv=indiv;
        l->slots[l->next_slot].event=event;
        l->slots[l->next_slot].isset=1;
        l->slots[l->next_slot].next=l->head;
        l->head=l->slots+l->next_slot;
        l->size++;
    } else {
        while(current->next != NULL && time > (current->next->time) ){
            current=current->next;
        }
        if(current->next!=NULL){
            l->slots[l->next_slot].time=time;
            l->slots[l->next_slot].indiv=indiv;
            l->slots[l->next_slot].event=event;
            l->slots[l->next_slot].isset=1;
            l->slots[l->next_slot].next=current->next;
            current->next=l->slots+l->next_slot;
            l->size++;
        } else {
            l->slots[l->next_slot].time=time;
            l->slots[l->next_slot].indiv=indiv;
            l->slots[l->next_slot].event=event;
            l->slots[l->next_slot].isset=1;
            l->slots[l->next_slot].next=NULL;
            current->next=l->slots+l->next_slot;
            l->size++;
        }
    }


    // Find next slot;
    int new_slot=l->next_slot;
    while(l->size < l->max_size){
        new_slot=(new_slot+1)%l->max_size;
        if(l->slots[new_slot].isset==0){
            l->next_slot=new_slot;
            return;
        }
    }

    // realloc slots or fail
    fprintf(stderr, "Slots are full, reallocation failed\n");
    exit(EXIT_FAILURE);
    /*
    new_size=((l->max_size)*1.5+1) > (l->max_capacity) ? l->max_capacity: ((l->max_size)*1.5+1);
    struct clocked_event *newslots=malloc(new_size*sizeof(struct clocked_event));
    if(newslots==NULL){
        fprintf(stderr, "Slots are full, reallocation failed\n");
        exit(EXIT_FAILURE);
    } else {
        fprintf(stderr, "realloced\n");
        memcpy(newslots, l->slots, l->max_size*sizeof(struct clocked_event));
        for(int i=l->max_size; i<new_size; i++){
            newslots[i].isset=0;
        }
        l->max_size=new_size;
        l->next_slot=l->size;
        free(l->slots);
        l->slots=newslots;
    }
    */
}

double getNextTime(struct list_ce *l){
    if(l->head!=NULL){
        return l->head->time;
    } else {
        fprintf(stderr, "Can't get time from empty list\n");
        exit(EXIT_FAILURE);
    }
}

