#include <R.h>
#include <Rdefines.h>
#include <Rinternals.h>
#include <Rmath.h>

#include <time.h>
#include "util.h"
#include "rates.h"


int simulation_loop(struct individuals *ind, 
    double *tcount, int *scount, int *icount, int *rcount, int *ecount, int *acount,
    int batch, int kmax, double tmax){
    double t=0;
    int ret=0;
    size_t i, event, k=0, l=0;

#ifndef RBUILD
    Rf_warning("Seed is set from current time -> NEEDS TO BE CHANGED ASAP");
    srand48(time(NULL));
#endif

	computeRatesSEAIR(ind);
	while(t<tmax && k<kmax && ret==0){
        R_CheckUserInterrupt();
		if(k%batch==0){
            tcount[l]=t;
            scount[l]=ind->counts[STATE_S];
            icount[l]=ind->counts[STATE_I];
            rcount[l]=ind->counts[STATE_R];
            ecount[l]=ind->counts[STATE_E];
            acount[l]=ind->counts[STATE_A];
            l++;
        }

		ret=selectEvent(ind, &t, &i, &event);
		if(ret==0){
			updateEventSEAIR(ind, i, event, t);
		}

		k++;
	}

	if((k-1)%batch!=0){
        tcount[l]=t;
        scount[l]=ind->counts[STATE_S];
        icount[l]=ind->counts[STATE_I];
        rcount[l]=ind->counts[STATE_R];
        ecount[l]=ind->counts[STATE_E];
        acount[l]=ind->counts[STATE_A];
        l++;
    }
    return l;
}

SEXP getListElement(SEXP list, const char *str)
{
    SEXP elmt = R_NilValue, names = getAttrib(list, R_NamesSymbol);

    for (int i = 0; i < length(list); i++)
        if(strcmp(CHAR(STRING_ELT(names, i)), str) == 0) {
           elmt = VECTOR_ELT(list, i);
           break;
        }
    return elmt;
}

SEXP simulate_seair(SEXP parameters, SEXP pop, SEXP clocks, SEXP rate_functions){
    double beta_i_i, beta_i_ih, beta_i_iw; // Infection S -> I, by I
    double beta_e_i, beta_e_ih, beta_e_iw; // Infection S -> E, by I
    double beta_e_a, beta_e_ah, beta_e_aw; // Infection S -> E, by A
    double param_ei, param_ea; // Symptomatic/asymptomatic rates
    double param_ir, param_ar; // Recovery rates from I and A
    int batchsize=1, itermax=1000;
    double tmax=1;

    // Initialise parameters
    beta_i_i = REAL(getListElement(parameters, "beta_i_i"))[0];
    beta_i_ih = REAL(getListElement(parameters, "beta_i_ih"))[0];
    beta_i_iw = REAL(getListElement(parameters, "beta_i_iw"))[0];

    beta_e_i = REAL(getListElement(parameters, "beta_e_i"))[0];
    beta_e_ih = REAL(getListElement(parameters, "beta_e_ih"))[0];
    beta_e_iw = REAL(getListElement(parameters, "beta_e_iw"))[0];

    beta_e_a = REAL(getListElement(parameters, "beta_e_a"))[0];
    beta_e_ah = REAL(getListElement(parameters, "beta_e_ah"))[0];
    beta_e_aw = REAL(getListElement(parameters, "beta_e_aw"))[0];

    param_ei = REAL(getListElement(parameters, "param_ei"))[0];
    param_ea = REAL(getListElement(parameters, "param_ea"))[0];
    param_ir = REAL(getListElement(parameters, "param_ir"))[0];
    param_ar = REAL(getListElement(parameters, "param_ar"))[0];

    itermax = INTEGER(getListElement(parameters, "itermax"))[0];
    tmax = REAL(getListElement(parameters, "tmax"))[0];
    batchsize = INTEGER(getListElement(parameters, "batchsize"))[0];

    // Read population
    PROTECT(pop);
    int *id=INTEGER(AS_INTEGER(getListElement(pop, "index")));
    int Nindiv=LENGTH(AS_INTEGER(getListElement(pop, "index")));
    int *state=INTEGER(AS_INTEGER(getListElement(pop, "C")));
    int *hindex=INTEGER(AS_INTEGER(getListElement(pop, "H")));
    int *windex=INTEGER(AS_INTEGER(getListElement(pop, "W")));

    // Simulation
    double *t;
    int *sc, *ic, *rc, *ec, *ac;
    
    // Initialise structure
    struct individuals ind;
    ind=initParametersSEAIR(
        beta_i_i, beta_i_ih, beta_i_iw,
        beta_e_i, beta_e_ih, beta_e_iw,
        beta_e_a, beta_e_ah, beta_e_aw,
        param_ei, param_ea,
        param_ir, param_ar);
    initPop(&ind, Nindiv, id, state, hindex, windex, NULL, NULL, NULL, NULL); 

    // Set the rate functions
    char rfunc[3];
    for(int i=0; i<3; i++){
        switch(INTEGER(rate_functions)[i]){
            case 0 :
                rfunc[i] = INFECTION_LIN;
                break;
            case 1 :
                rfunc[i] = INFECTION_SQRT;
                break;
            default :
                rfunc[i] = INFECTION_LIN;
        }
    }
    set_infection_rate_functions(&ind, rfunc[0], rfunc[1], rfunc[2]);

    // Call simulation loop
    int size=Nindiv*3/batchsize;
    t=malloc(size*sizeof(double));
    sc=malloc(size*sizeof(int));
    ic=malloc(size*sizeof(int));
    rc=malloc(size*sizeof(int));
    ec=malloc(size*sizeof(int));
    ac=malloc(size*sizeof(int));
    size=simulation_loop(&ind, t, sc, ic, rc, ec, ac, batchsize, itermax, tmax);
    
    // Convert output to R format
    SEXP tsexp = PROTECT(allocVector(REALSXP, size));
    SEXP ssexp = PROTECT(allocVector(INTSXP, size));
    SEXP isexp = PROTECT(allocVector(INTSXP, size));
    SEXP rsexp = PROTECT(allocVector(INTSXP, size));
    SEXP esexp = PROTECT(allocVector(INTSXP, size));
    SEXP asexp = PROTECT(allocVector(INTSXP, size));

    int index;
    for(index=0; index<size; index++){
        REAL(tsexp)[index]=t[index];
        INTEGER(ssexp)[index]=sc[index];
        INTEGER(isexp)[index]=ic[index];
        INTEGER(rsexp)[index]=rc[index];
        INTEGER(esexp)[index]=ec[index];
        INTEGER(asexp)[index]=ac[index];
    }
    free(t);
    free(sc);
    free(ic);
    free(rc);
    free(ec);
    free(ac);

	freeIndividuals(&ind);

    // Return
    const char *names[] = {"t", "S", "I", "R", "E", "A", ""};
    SEXP result = PROTECT(Rf_mkNamed(VECSXP, names));
    SET_VECTOR_ELT(result, 0, tsexp);
    SET_VECTOR_ELT(result, 1, ssexp);
    SET_VECTOR_ELT(result, 2, isexp);
    SET_VECTOR_ELT(result, 3, rsexp);
    SET_VECTOR_ELT(result, 4, esexp);
    SET_VECTOR_ELT(result, 5, asexp);
    UNPROTECT(8);
    return result;
}
