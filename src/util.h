#define UTIL_H

#ifndef CLOCKED_H
#include "clocked.h"
#endif

#define MAXLINE 1500
#define MAXSTRUCT 50

#define NSTATES 5
#define STATE_S 0
#define STATE_I 1
#define STATE_R 2
#define STATE_E 3
#define STATE_A 4

#define NEVENTS 10
#define EVENT_INFE 0 // S -> I, mean field infection
#define EVENT_INFH 1 // S -> I, household infection
#define EVENT_INFW 2 // S -> I, workplace infection
#define EVENT_INFE_SE 4 // S -> E, mean field infection
#define EVENT_INFH_SE 5 // S -> E, household infection
#define EVENT_INFW_SE 6 // S -> E, workplace infection
#define EVENT_EI 7 // E -> I
#define EVENT_EA 8 // E -> A
#define EVENT_AR 9 // A -> R
#define EVENT_RECO 3 // I -> R
#define EVENT_IR 3 // I -> R

#define INDIV_CM 0
#define INDIV_H  1
#define INDIV_W  2

#define INFECTION_LIN 'L'
#define INFECTION_SQRT 'S'

#define NGEN 50

struct structure {
	size_t max;
	size_t *N;
	size_t **ind;
};

struct individuals {
	size_t N;
	size_t counts[NSTATES];
	size_t *index;
	short *state;
	double *rates[NEVENTS];
	double ratesByEvent[NEVENTS];
	size_t *nh[NSTATES];
	size_t *nw[NSTATES];
	size_t *h;
	size_t *w;

	// Generation counting
	short *generation;
	size_t genCounts[NGEN]; 
	size_t *genh[NGEN];
	size_t *genw[NGEN];

	// Structure Generation counting
	short *generationStructure;
	short *generationStructureType;
	size_t genCountsStructure[NGEN*3]; 
	size_t *genhStructure[NGEN*3];
	size_t *genwStructure[NGEN*3];

    // Parameters
    // REMOVE ME
	double beta, betaH, betaW, gamma;

    double beta_i_i, beta_i_ih, beta_i_iw; // Infection S -> I, by I
    double beta_e_i, beta_e_ih, beta_e_iw; // Infection S -> E, by I
    double beta_e_a, beta_e_ah, beta_e_aw; // Infection S -> E, by A
    double param_ei, param_ea; // Symptomatic/asymptomatic rates
    double param_ir, param_ar; // Recovery rates from I and A

    // Infection rate function
    char infection_rate_household, infection_rate_workplace, infection_rate_general;

    // Clocked events
    int with_clocks;
    double *infection_time;
    double *duration_ei, *duration_ea, *duration_ir, *duration_ar;
    struct list_ce clocked_event_list;

	struct structure households;
	struct structure workplaces;
};

struct individuals initParameters(double beta, double betaH, double betaW, double gamma);

struct individuals initParametersSEAIR(double beta_i_i, double beta_i_ih, double beta_i_iw,
    double beta_e_i, double beta_e_ih, double beta_e_iw,
    double beta_e_a, double beta_e_ah, double beta_e_aw,
    double param_ei, double param_ea,
    double param_ir, double param_ar);

void set_infection_rate_functions(struct individuals *ind, char general, char household, char workplace);

struct individuals fileInitParametersSEAIR(char *file);

void freeIndividuals(struct individuals *ind);

void readPop(char *file, struct individuals *ind);
void initPop(struct individuals *ind, int N, int *id, int *state, int *h, int *w,
    double *clock_time_ei, double *clock_time_ea, double *clock_time_ir, double *clock_time_ar);


size_t getHSize(struct individuals *ind, size_t i);
size_t getWSize(struct individuals *ind, size_t i);

short getHState(struct individuals *ind, size_t h, size_t j);
short getWState(struct individuals *ind, size_t w, size_t j);


void print(struct individuals *ind);
void printSummary(struct individuals *ind, double t);
void printStructures(struct individuals *ind);
void writeDyn(struct individuals *ind, double t, FILE* fd);


short selectGen(size_t *counts);
short selectGenStruct(size_t **counts, size_t k);
void selectGenStructureCM(size_t *counts, size_t *gen, short *type);
void selectGenStructureHW(size_t **counts, size_t l, size_t *gen, short *type);
