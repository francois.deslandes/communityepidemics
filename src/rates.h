#define RATES_H
#include <stdio.h>

#ifndef UTIL_H
#include "util.h"
#endif

#ifndef CLOCKED_H
#include "clocked.h"
#endif


double getRateInfectionE(struct individuals *ind, size_t i);
double getRateInfectionH(struct individuals *ind, size_t i);
double getRateInfectionW(struct individuals *ind, size_t i);

double getRateInfectionE_SE(struct individuals *ind, size_t i);
double getRateInfectionH_SE(struct individuals *ind, size_t i);
double getRateInfectionW_SE(struct individuals *ind, size_t i);

double getRateEI(struct individuals *ind, size_t i);
double getRateEA(struct individuals *ind, size_t i);

double getRateRecovery(struct individuals *ind, size_t i);
double getRateAR(struct individuals *ind, size_t i);

void computeRatesInd(struct individuals *ind, size_t i);
void computeRatesIndSEAIR(struct individuals *ind, size_t i);
void computeRates(struct individuals *ind);
void computeRatesSEAIR(struct individuals *ind);
double getGlobalRate(struct individuals *ind);
int selectEvent(struct individuals *ind, double *t, size_t *i, size_t *e);
void updateEvent(struct individuals *ind, size_t i, size_t e);
void updateEventSEAIR(struct individuals *ind, size_t i, size_t e, double t);
