#include <math.h>
#include <stdlib.h>

#include "rates.h"

#ifdef RBUILD
#include <R.h>
#include <Rdefines.h>
#include <Rinternals.h>
#include <Rmath.h>
#define drand48() runif(0, 1)
#undef beta
#endif

double getRateInfectionE(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_S){
        switch(ind->infection_rate_general){
            case INFECTION_SQRT:
		        rate=ind->beta*sqrt(ind->counts[STATE_I]/ind->N);
                break;
            case INFECTION_LIN:
		        rate=ind->beta*ind->counts[STATE_I]/ind->N;
                break;
        }
	}
	return rate;
}

double getRateInfectionH(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_S){
        switch(ind->infection_rate_household){
            case INFECTION_SQRT:
		        rate=(ind->betaH)*sqrt(ind->nh[STATE_I][i]);
                break;
            case INFECTION_LIN:
		        rate=(ind->betaH)*(ind->nh[STATE_I][i]);
                break;
        }
	}
	return rate;
}

double getRateInfectionW(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_S){
        switch(ind->infection_rate_household){
            case INFECTION_SQRT:
		        rate=(ind->betaW)*sqrt(ind->nw[STATE_I][i]);
                break;
            case INFECTION_LIN:
		        rate=(ind->betaW)*(ind->nw[STATE_I][i]);
                break;
        }
	}
	return rate;
}

double getRateInfectionE_SE(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_S){
        switch(ind->infection_rate_general){
            case INFECTION_SQRT:
		            rate+=ind->beta_e_i*sqrt(ind->counts[STATE_I]/ind->N);
		            rate+=ind->beta_e_a*sqrt(ind->counts[STATE_A]/ind->N);
                break;
            case INFECTION_LIN:
		            rate+=ind->beta_e_i*ind->counts[STATE_I]/ind->N;
		            rate+=ind->beta_e_a*ind->counts[STATE_A]/ind->N;
                break;
        }
	}
	return rate;
} 

double getRateInfectionH_SE(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_S){
        switch(ind->infection_rate_general){
            case INFECTION_SQRT:
		        rate+=ind->beta_e_ih*sqrt(ind->nh[STATE_I][i]);
		        rate+=ind->beta_e_ah*sqrt(ind->nh[STATE_A][i]);
                break;
            case INFECTION_LIN:
		        rate+=ind->beta_e_ih*(ind->nh[STATE_I][i]);
		        rate+=ind->beta_e_ah*(ind->nh[STATE_A][i]);
                break;
        }
	}
	return rate;
}

double getRateInfectionW_SE(struct individuals *ind, size_t i){ 
	double rate=0.0;
	if(ind->state[i]==STATE_S){
        switch(ind->infection_rate_general){
            case INFECTION_SQRT:
		            rate+=ind->beta_e_iw*sqrt(ind->nw[STATE_I][i]);
		            rate+=ind->beta_e_aw*sqrt(ind->nw[STATE_A][i]);
                break;
            case INFECTION_LIN:
		            rate+=ind->beta_e_iw*(ind->nw[STATE_I][i]);
		            rate+=ind->beta_e_aw*(ind->nw[STATE_A][i]);
                break;
        }
	}
	return rate;
}

double getRateEI(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_E){
		rate=ind->param_ei;
	}
	return rate;
}

double getRateEA(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_E){
		rate=ind->param_ea;
	}
	return rate;
}

double getRateRecovery(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_I){
		rate=ind->gamma;
	}
	return rate;
}

double getRateAR(struct individuals *ind, size_t i){
	double rate=0.0;
	if(ind->state[i]==STATE_A){
		rate=ind->param_ar;
	}
	return rate;
}

void computeRatesInd(struct individuals *ind, size_t i){
	ind->rates[EVENT_INFE][i]=getRateInfectionE(ind, i);
	ind->rates[EVENT_INFH][i]=getRateInfectionH(ind, i);
	ind->rates[EVENT_INFW][i]=getRateInfectionW(ind, i);
	ind->rates[EVENT_RECO][i]=getRateRecovery(ind, i);
}

void computeRatesIndSEAIR(struct individuals *ind, size_t i){
	ind->rates[EVENT_INFE][i]=getRateInfectionE(ind, i);
	ind->rates[EVENT_INFH][i]=getRateInfectionH(ind, i);
	ind->rates[EVENT_INFW][i]=getRateInfectionW(ind, i);

	ind->rates[EVENT_INFE_SE][i]=getRateInfectionE_SE(ind, i);
	ind->rates[EVENT_INFH_SE][i]=getRateInfectionH_SE(ind, i);
	ind->rates[EVENT_INFW_SE][i]=getRateInfectionW_SE(ind, i);

	ind->rates[EVENT_EI][i]=getRateEI(ind, i);
	ind->rates[EVENT_EA][i]=getRateEA(ind, i);

	ind->rates[EVENT_RECO][i]=getRateRecovery(ind, i);
	ind->rates[EVENT_AR][i]=getRateAR(ind, i);
}

void computeRates(struct individuals *ind){
	size_t i, k;
	for(k=0; k<NEVENTS; k++){
		ind->ratesByEvent[k]=0.0;
	}
	for(i=0; i<ind->N; i++){
		computeRatesInd(ind, i);
		for(k=0; k<NEVENTS; k++){
			ind->ratesByEvent[k]+=ind->rates[k][i];
		}
	}
}

void computeRatesSEAIR(struct individuals *ind){
	size_t i, k;

	for(k=0; k<NEVENTS; k++){
		ind->ratesByEvent[k]=0.0;
	}
	for(i=0; i<ind->N; i++){
		computeRatesIndSEAIR(ind, i);
		for(k=0; k<NEVENTS; k++){
			ind->ratesByEvent[k]+=ind->rates[k][i];
		}
	}
}

double getGlobalRate(struct individuals *ind){
	double R=0.0;
	size_t i, j;
	for(i=0; i<ind->N; i++){
		for(j=0; j<NEVENTS; j++){
			//R+=ind->rates[j][i];
            // All global rates may not be updated for speed reasons
            if(j==EVENT_INFE){
			    R+=getRateInfectionE(ind, i);
            } else if(j==EVENT_INFE_SE){
			    R+=getRateInfectionE_SE(ind, i);
            } else {
			    R+=ind->rates[j][i];
            }
		}
	}
	return R;
}

int selectEvent(struct individuals *ind, double *t, size_t *i, size_t *e){
	double alpha, newt;
	double R;
	double runningR=0.0;
    double next_clocked_event;
	size_t k, j;

    // Rate events
    R=getGlobalRate(ind);
	newt=*t+log(1.0/drand48())/R;

    // Clocked event
    if(ind->with_clocks && ind->clocked_event_list.size>0){
        next_clocked_event=getNextTime(&ind->clocked_event_list);

        if(next_clocked_event<newt){
            popCE(&ind->clocked_event_list, t, i, e);
            return 0;
        }
    }

	alpha=drand48()*R;
	for(k=0; k<ind->N; k++){
		for(j=0; j<NEVENTS; j++){
            // Check if global event (rates may not be udpdated for global events (for speed)
            if(j==EVENT_INFE){
			    runningR+=getRateInfectionE(ind, k);
            } else if(j==EVENT_INFE_SE){
			    runningR+=getRateInfectionE_SE(ind, k);
            } else {
			    runningR+=ind->rates[j][k];
            }
			if(runningR>alpha){
				*i=k;
				*e=j;
				*t=newt;
				return 0;
			}
		}
	}

	return 1;
}


void updateEvent(struct individuals *ind, size_t i, size_t e){
	size_t k, j, h, w, s;
	size_t gen_infecting, genStructure_infecting;
	short type, typeStructure_infecting;
	short delta_gen, delta_gen_struct;
	short update[NSTATES];

	switch(e){ 
		case EVENT_INFE:
			// Generation counting
			gen_infecting=selectGen(ind->genCounts);
			ind->generation[i]=gen_infecting+1;
			delta_gen=+1;

			// Structure Generation counting
			selectGenStructureCM(ind->genCountsStructure, &genStructure_infecting, &typeStructure_infecting);
			delta_gen_struct=+1;
			switch(typeStructure_infecting){
				case INDIV_CM:
					ind->generationStructure[i]=genStructure_infecting+1;
					ind->generationStructureType[i]=INDIV_CM;
					break;
				case INDIV_H:
					ind->generationStructure[i]=genStructure_infecting+1;
					ind->generationStructureType[i]=INDIV_CM;
					break;
				case INDIV_W:
					ind->generationStructure[i]=genStructure_infecting+1;
					ind->generationStructureType[i]=INDIV_CM;
					break;
			}


			// New state
			ind->state[i]=STATE_I;

			// Update for counters
			update[STATE_S]=-1;
			update[STATE_I]=+1;
			update[STATE_R]=+0;
			break;
		case EVENT_INFH:
			// Generation counting
			gen_infecting=selectGenStruct(ind->genh, i);
			ind->generation[i]=gen_infecting+1;
			delta_gen=+1;

			// Structure Generation counting
			selectGenStructureHW(ind->genhStructure, i, &genStructure_infecting, &typeStructure_infecting);
			delta_gen_struct=+1;
			switch(typeStructure_infecting){
				case INDIV_CM:
					ind->generationStructure[i]=genStructure_infecting;
					ind->generationStructureType[i]=INDIV_H;
					break;
				case INDIV_H:
					ind->generationStructure[i]=genStructure_infecting;
					ind->generationStructureType[i]=INDIV_H;
					break;
				case INDIV_W:
					ind->generationStructure[i]=genStructure_infecting+1;
					ind->generationStructureType[i]=INDIV_H;
					break;
			}

			// New state
			ind->state[i]=STATE_I;

			// Update for counters
			update[STATE_S]=-1;
			update[STATE_I]=+1;
			update[STATE_R]=+0;
			break;
		case EVENT_INFW:
			// Generation counting
			gen_infecting=selectGenStruct(ind->genw, i);
			ind->generation[i]=gen_infecting+1;
			delta_gen=+1;

			// Structure Generation counting
			selectGenStructureHW(ind->genwStructure, i, &genStructure_infecting, &typeStructure_infecting);
			delta_gen_struct=+1;
			switch(typeStructure_infecting){
				case INDIV_CM:
					ind->generationStructure[i]=genStructure_infecting;
					ind->generationStructureType[i]=INDIV_W;
					break;
				case INDIV_H:
					ind->generationStructure[i]=genStructure_infecting+1;
					ind->generationStructureType[i]=INDIV_W;
					break;
				case INDIV_W:
					ind->generationStructure[i]=genStructure_infecting;
					ind->generationStructureType[i]=INDIV_W;
					break;
			}

			// New state
			ind->state[i]=STATE_I;

			// Update for counters
			update[STATE_S]=-1;
			update[STATE_I]=+1;
			update[STATE_R]=+0;
			break;
		case EVENT_RECO:
			// Generation counting
			delta_gen=-1;

			// Structure Generation counting
			delta_gen_struct=-1;

			// New state
			ind->state[i]=STATE_R;

			// Update for counters
			update[STATE_S]=+0;
			update[STATE_I]=-1;
			update[STATE_R]=+1;
			break;
	}


	// Generation counting
	if(ind->generation[i]>=0 && ind->generation[i]<NGEN){
		ind->genCounts[ind->generation[i]]+=delta_gen;
	}

	// Structure Generation counting
	if(ind->generationStructure[i]>=0 && ind->generationStructure[i]<NGEN){
		type=ind->generationStructureType[i];
		ind->genCountsStructure[ind->generationStructure[i]+type*NGEN]+=delta_gen;
	}

	for(s=0; s<NSTATES; s++){
		ind->counts[s]+=update[s];
	}

	// update household count for generations
	h=ind->h[i];
	for(k=0; k<getHSize(ind, i); k++){
		j=ind->households.ind[h][k];
		
		// Generation counting
		if(ind->generation[i]>=0 && ind->generation[i]<NGEN){
			ind->genh[ind->generation[i]][j]+=delta_gen;
		}

		// Structure Generation counting
		if(ind->generationStructure[i]>=0 && ind->generationStructure[i]<NGEN){
			type=ind->generationStructureType[i];
			ind->genhStructure[ind->generationStructure[i]+type*NGEN][j]+=delta_gen;
		}

		for(s=0; s<NSTATES; s++){
			ind->nh[s][j]+=update[s];
		}
		computeRatesInd(ind, j);
	}

	// update workplace count for generations
	w=ind->w[i];
	for(k=0; k<getWSize(ind, i); k++){
		j=ind->workplaces.ind[w][k];

		// Generation counting
		if(ind->generation[i]>=0 && ind->generation[i]<NGEN)
			ind->genw[ind->generation[i]][j]+=delta_gen;

		// Structure Generation counting
		if(ind->generationStructure[i]>=0 && ind->generationStructure[i]<NGEN){
			type=ind->generationStructureType[i];
			ind->genwStructure[ind->generationStructure[i]+type*NGEN][j]+=delta_gen;
		}

		for(s=0; s<NSTATES; s++){
			ind->nw[s][j]+=update[s];
		}
		computeRatesInd(ind, j);
	}

	// recompute external infection rate for all
	for(k=0; k<ind->N; k++){
		if(ind->state==STATE_S){
			ind->rates[EVENT_INFE][k]=(ind->beta)*(ind->counts[STATE_I])/ind->N;
		} else {
			ind->rates[EVENT_INFE][k]=0.0;
		}
	}

}


void updateEventSEAIR(struct individuals *ind, size_t i, size_t e, double t){
	size_t k, j, h, w, s;
	size_t gen_infecting, genStructure_infecting;
	short type, typeStructure_infecting;
	short delta_gen, delta_gen_struct;
	short update[NSTATES];

	switch(e){ 
        // infection S->I, by I
		case EVENT_INFE:
		case EVENT_INFH:
		case EVENT_INFW:
			// New state
			ind->state[i]=STATE_I;

            // Clocked event
            ind->infection_time[i]=t;
            if(ind->duration_ei[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ei[i], i, EVENT_EI);

            if(ind->duration_ea[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ea[i], i, EVENT_EA);

            if(ind->duration_ir[i]>0 && ind->duration_ei[i]<0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ir[i], i, EVENT_IR);

            if(ind->duration_ir[i]>0 && ind->duration_ei[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ir[i]+ind->duration_ei[i], i, EVENT_IR);

            if(ind->duration_ar[i]>0 && ind->duration_ea[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ar[i]+ind->duration_ea[i], i, EVENT_AR);

			// Update for counters
			update[STATE_S]= -1;
			update[STATE_I]= +1;
			update[STATE_R]= +0;
			update[STATE_E]= +0;
			update[STATE_A]= +0;
            break;

        // infection S->E, by I or A
		case EVENT_INFE_SE:
		case EVENT_INFH_SE:
		case EVENT_INFW_SE:
			// New state
			ind->state[i]=STATE_E;

            // Clocked event
            ind->infection_time[i]=t;
            if(ind->duration_ei[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ei[i], i, EVENT_EI);

            if(ind->duration_ea[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ea[i], i, EVENT_EA);

            if(ind->duration_ir[i]>0 && ind->duration_ei[i]<0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ir[i], i, EVENT_IR);

            if(ind->duration_ir[i]>0 && ind->duration_ei[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ir[i]+ind->duration_ei[i], i, EVENT_IR);

            if(ind->duration_ar[i]>0 && ind->duration_ea[i]>0)
                insertIncreasingListCE(&ind->clocked_event_list, t+ind->duration_ar[i]+ind->duration_ea[i], i, EVENT_AR);

			// Update for counters
			update[STATE_S]= -1;
			update[STATE_I]= +0;
			update[STATE_R]= +0;
			update[STATE_E]= +1;
			update[STATE_A]= +0;
            break;

        // Transition E->I and E->A 
		case EVENT_EI:
			// New state
			ind->state[i]=STATE_I;

			// Update for counters
			update[STATE_S]= +0;
			update[STATE_I]= +1;
			update[STATE_R]= +0;
			update[STATE_E]= -1;
			update[STATE_A]= +0;
            break;
		case EVENT_EA:
			// New state
			ind->state[i]=STATE_A;

			// Update for counters
			update[STATE_S]= +0;
			update[STATE_I]= +0;
			update[STATE_R]= +0;
			update[STATE_E]= -1;
			update[STATE_A]= +1;
            break;

        // Transition I->R and A->R
		case EVENT_RECO:
			// New state
			ind->state[i]=STATE_R;

			// Update for counters
			update[STATE_S]= +0;
			update[STATE_I]= -1;
			update[STATE_R]= +1;
			update[STATE_E]= +0;
			update[STATE_A]= +0;
            break;
		case EVENT_AR:
			// New state
			ind->state[i]=STATE_R;

			// Update for counters
			update[STATE_S]= +0;
			update[STATE_I]= +0;
			update[STATE_R]= +1;
			update[STATE_E]= +0;
			update[STATE_A]= -1;
            break;
    }


    // Update global counters
	for(s=0; s<NSTATES; s++){
		ind->counts[s]+=update[s];
	}
    // --> Update the global rate

	// Update household counters
	h=ind->h[i];
	for(k=0; k<getHSize(ind, i); k++){
		j=ind->households.ind[h][k];

		for(s=0; s<NSTATES; s++){
			ind->nh[s][j]+=update[s];
		}
		computeRatesIndSEAIR(ind, j);
        // --> Update the global rate
    }

	// Update workplace counters
	w=ind->w[i];
	for(k=0; k<getWSize(ind, i); k++){
		j=ind->workplaces.ind[w][k];

		for(s=0; s<NSTATES; s++){
			ind->nw[s][j]+=update[s];
		}
		computeRatesIndSEAIR(ind, j);
        // --> Update the global rate
    }
}

short selectGen(size_t *counts){
	double alpha=drand48();
	short gen=NGEN+1;
	int sum, total, i;
	total=0;
	for(i=0; i<NGEN; i++){
		total+=counts[i];
	}
	if(total==0){
		return NGEN+1;
	}

	sum=0;
	for(i=0; i<NGEN; i++){
		sum+=counts[i];
		if(alpha<sum*1.0/total){
			gen=i;
			break;
		}
	}

	return gen;
}

short selectGenStruct(size_t **counts, size_t k){
	double alpha=drand48();
	short gen=NGEN+1;
	int sum, total, i;
	total=0;
	for(i=0; i<NGEN; i++){
		total+=counts[i][k];
	}
	if(total==0){
		return NGEN+1;
	}

	sum=0;
	for(i=0; i<NGEN; i++){
		sum+=counts[i][k];
		if(alpha<sum*1.0/total){
			gen=i;
			break;
		}
	}

	return gen;
}


void selectGenStructureCM(size_t *counts, size_t *gen, short *type){
	double alpha=drand48();
	int sum, total, i, k;
	*gen=NGEN+1;
	total=0;
	for(i=0; i<3*NGEN; i++){
		total+=counts[i];
	}
	if(total==0){
		*gen=NGEN+1;
		*type=-1;
		return;
	}

	sum=0;
	for(k=0; k<3; k++){
		for(i=0; i<NGEN; i++){
			sum+=counts[i+k*NGEN];
			if(alpha<sum*1.0/total){
				*gen=i;
				*type=k;
				return;
			}
		}
	}

}


void selectGenStructureHW(size_t **counts, size_t l, size_t *gen, short *type){
	double alpha=drand48();
	int sum, total, i, k;
	*gen=NGEN+1;
	total=0;
	for(i=0; i<3*NGEN; i++){
		total+=counts[i][l];
	}
	if(total==0){
		*gen=NGEN+1;
		*type=-1;
	}

	sum=0;
	for(k=0; k<3; k++){
		for(i=0; i<NGEN; i++){
			sum+=counts[i+k*NGEN][l];
			if(alpha<sum*1.0/total){
				*gen=i;
				*type=k;
				return;
			}
		}
	}

}
