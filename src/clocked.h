#define CLOCKED_H

struct clocked_event {
    short isset;
    double time;
    size_t indiv;
    size_t event;
    struct clocked_event *next;
};

struct list_ce {
    struct clocked_event *head;
    size_t size;
    size_t max_size;
    size_t max_capacity;
    size_t next_slot;
    struct clocked_event *slots;
};

void initListCE(struct list_ce *l, size_t max_size, size_t max_capacity);
void freeListCE(struct list_ce *l);
void popCE(struct list_ce *l, double *time, size_t *indiv, size_t *event);
void insertIncreasingListCE(struct list_ce *l, double time, size_t indiv, size_t event);
double getNextTime(struct list_ce *l);
//void printlist(struct list *l);
